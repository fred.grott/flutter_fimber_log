Run Flutter unit tests for the current project.

Global options:
-h, --help                  Print this usage information.
-v, --verbose               Noisy logging, including all shell commands executed.
                            If used with --help, shows hidden options.
-d, --device-id             Target device id or name (prefixes allowed).
    --version               Reports the version of this tool.
    --suppress-analytics    Suppress analytics reporting when this command runs.

Usage: flutter test [arguments]
-h, --help                            Print this usage information.
    --[no-]pub                        Whether to run "flutter pub get" before executing this command.
                                      (defaults to on)
    --[no-]null-assertions            Perform additional null assertions on the boundaries of migrated and un-migrated code. This setting is not currently supported on desktop devices.
    --[no-]track-widget-creation      Track widget creation locations. This enables features such as the widget inspector. This parameter is only functional in debug mode (i.e. when compiling JIT, not AOT).
                                      (defaults to on)
    --dart-define=<foo=bar>           Additional key-value pairs that will be available as constants from the String.fromEnvironment, bool.fromEnvironment, int.fromEnvironment, and double.fromEnvironment constructors.
                                      Multiple defines can be passed by repeating --dart-define multiple times.
    --web-renderer                    The renderer implementation to use when building for the web. Possible values are:
                                      html - always use the HTML renderer. This renderer uses a combination of HTML, CSS, SVG, 2D Canvas, and WebGL. This is the default.
                                      canvaskit - always use the CanvasKit renderer. This renderer uses WebGL and WebAssembly to render graphics.
                                      auto - use the HTML renderer on mobile devices, and CanvasKit on desktop devices.
                                      [auto (default), canvaskit, html]
    --name=<regexp>                   A regular expression matching substrings of the names of tests to run.
    --plain-name=<substring>          A plain-text substring of the names of tests to run.
-t, --tags                            Run only tests associated with tags
-x, --exclude-tags                    Run only tests WITHOUT given tags
    --start-paused                    Start in a paused mode and wait for a debugger to connect.
                                      You must specify a single test file to run, explicitly.
                                      Instructions for connecting with a debugger are printed to the console once the test has started.
    --coverage                        Whether to collect coverage information.
    --merge-coverage                  Whether to merge coverage data with "coverage/lcov.base.info".
                                      Implies collecting coverage data. (Requires lcov)
    --coverage-path                   Where to store coverage information (if coverage is enabled).
                                      (defaults to "coverage/lcov.info")
    --update-goldens                  Whether matchesGoldenFile() calls within your test methods should update the golden files rather than test for an existing match.
-j, --concurrency=<jobs>              The number of concurrent test processes to run.
                                      (defaults to "6")
    --[no-]test-assets                Whether to build the assets bundle for testing.
                                      Consider using --no-test-assets if assets are not required.
                                      (defaults to on)
    --test-randomize-ordering-seed    The seed to randomize the execution order of test cases.
                                      Must be a 32bit unsigned integer or "random".
                                      If "random", pick a random seed to use.
                                      If not passed, do not randomize test case execution order.
-r, --reporter                        Set how to print test results.
                                      [compact] (default)         A single line, updated continuously.
                                      [expanded]                  A separate line for each update.
                                      [json]                      A machine-readable format (see https://dart.dev/go/test-docs/json_reporter.md).
                                      (defaults to "compact")
    --timeout                         The default test timeout. For example: 15s, 2x, none. Defaults to "30s"
                                      (defaults to "30s")
    --dds-port                        When this value is provided, the Dart Development Service (DDS) will be bound to the provided port.
                                      Specifying port 0 (the default) will find a random free port.

Run "flutter help" to see global options.
