// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license.

// This is slightly non-pure stuff so it has to be here

import 'package:fimber/fimber.dart';
import 'package:fimber_io/fimber_io.dart';
import 'package:flutter/widgets.dart';
import 'package:path_provider/path_provider.dart';

// setting tag for the whole app
FimberLog logger = FimberLog("flutter_log_fimber");

Fimber plantMeDebug() {
  Fimber.plantTree(DebugTree.elapsed(useColors: true));

  return Fimber();
}

Future<String> getLocalPath() async {
  WidgetsFlutterBinding.ensureInitialized();
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<Fimber> plantFileTree() async {
  final String tempPath = await getLocalPath();

  // ignore: await_only_futures
  await Fimber.plantTree(FimberFileTree(
    "${tempPath}my-Log-File.txt",
    logFormat:
        "${CustomFormatTree.timeElapsedToken} ${CustomFormatTree.messageToken} ${CustomFormatTree.timeStampToken}",
  ));

  return Fimber();
}
