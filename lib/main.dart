import 'package:fimber/fimber.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber_log/my_log_setup.dart';

import 'presentation/my_app.dart';


// In Domain Driven Development terms you would move the other
// widget presentation classes to the presentation sub-folder
// ignore: avoid_void_async
void main() async {

  // timing and colors defined in function
  plantMeDebug();

  // ignore: no-empty-block
  if (kIsWeb) {
  // running on the web!
  } else {
    await plantFileTree();
  }


  

  // just showing how to do log blocks without our tag
  Fimber.withTag("TEST BLOCK", (log) {
    log.d("Started block");
    for (var i = 0; i >= 1; i++) {
      log.d("value: $i");
    }
    log.i("End of block");
  });



  runApp(MyApp());
}


