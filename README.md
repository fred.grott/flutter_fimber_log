# Flutter Fimber Log

![expert flutter logging](./images/expert-flutter-logging.png)

[**Explore the docs**](https://gitlab.com/fred.grott/flutteer_fimber_log)

![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/24942735?license=true&query=license.name&colorB=yellow)
![Created at](https://img.shields.io/badge/dynamic/json.svg?label=Created%20at&url=https://gitlab.com/api/v4/projects/24942735&query=created_at&colorB=informational)
![Last activity](https://img.shields.io/badge/dynamic/json.svg?label=Last%20activity&url=https://gitlab.com/api/v4/projects/24942735&query=last_activity_at&colorB=informational)
[![Contributors](https://badgen.net/gitlab/contributors/fred.grott/flutter_fimber_log)](https://gitlab.com/fred.grott/flutter_fimber_log/-/graphs/master)
[![Starrers](https://badgen.net/gitlab/stars/fred.grott/flutter_fimber_log)](https://gitlab.com/fred.grott/flutter_fimber_log/-/starrers)
[![Forks](https://badgen.net/gitlab/forks/fred.grott/flutter_fimber_log)](https://gitlab.com/fred.grott/flutter_fimber_log/-/forks)
[![Open Issues](https://badgen.net/gitlab/open-issues/fred.grott/flutter_fimber_log)](https://gitlab.com/fred.grott/flutter_fimber_log/-/issues)

## Table Of Contents

- [Flutter Fimber Log](#flutter-fimber-log)
  - [Table Of Contents](#table-of-contents)
    - [About The Project](#about-the-project)
      - [Built With](#built-with)
    - [Getting Started](#getting-started)
      - [Prerequisites](#prerequisites)
      - [Installation](#installation)
    - [Usage](#usage)
    - [Roadmap](#roadmap)
    - [Contributing](#contributing)
    - [License](#license)
    - [Contact](#contact)
    - [Acknowledgements](#acknowledgements)
    - [Specific Resources](#specific-resources)
    - [Flutter Community Resources](#flutter-community-resources)

### About The Project

This flutter_fimber_log project shows how to properly log errors in a flutter application using the Fimber Flutter set of log plugins.

#### Built With

This flutter project is built with:

- [Android Studio](https://developer.android.com/studio/preview)
- [Microsoft's Visual Studio Code](https://code.visualstudio.com/insiders/)
- [Flutter Front-End Framework SDK](https://flutter.dev/docs/get-started/install)

### Getting Started

To get started you need to download the Flutter SDK via:

[Flutter SDK](https://flutter.dev/docs/get-started/install)

Since, version Flutter 2.x you only need to have one browser installed although if you are targeting some platforms such as mobile you need to install those SDKs. The easiest way for android sdks is to install Android Studio and in the case of iOS SDks install the XCode IDE.

#### Prerequisites

The only pre-requisites is to have the Flutter sdk installed and one target platform sdk installed.

#### Installation

To install this git repo locally to modify and play with its this terminal command:

```bash
git clone https://gitlab.com/fred.gtott/flutter_fimber_log.git
```

### Usage

Just copy the example and modify it to use in your own flutter app. And, it might help to read the article linked in the resources section.

### Roadmap

No real roadmap as most of it's in final form.

### Contributing

At the moment, I am not accepting contributions. But, you can still fork it and modify it.

### License

Distributed under the BSD License. See [LICENSE](https://gitlab.com/fred.grott/flutter_fimber_log/-/blob/master/LICENSE) for more information.

### Contact

Fred Grott [@twitter_handle](https://twitter.com/fredgrott) - email: fred DOT grott AT gmail DOT com

Project Link: [https://gitlab.com/fred.grott/flutter_fimber_log](https://gitlab.com/fred.grott/flutter_fimber_log)

### Acknowledgements

I want to thank Mateusz Perlak for creating the Fimber set of log flutter plugins.

### Specific Resources

- [Flutter Perfect SetUp](https://medium.com/codex/flutter-perfect-setup-c5462b412f78)
  
- [Expert Flutter IDE SetUp](https://medium.com/codex/expert-flutter-ide-setup-57b3768ee351)
  
### Flutter Community Resources

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)

- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
  
- [Flutter online documentation](https://flutter.dev/docs)
  
- [Dart online documentation](https://dart.dev/docs)

- [Ask Flutter Dev questions@Stackoverflow](https://stackoverflow.com/tags/flutter)
  
- [Ask Flutter Dev questions@Reddit](https://www.reddit.com/r/FlutterDev/)
  
- [Flutter Community Articles@Medium.com](https://medium.com/flutter-io)
  
- [Solido Awesome Flutter List Of Resources](https://github.com/Solido/awesome-flutter)
  
- [Flutter Dev Videos@Youtube](https://www.youtube.com/playlist?list=PLOU2XLYxmsIJ7dsVN4iRuA7BT8XHzGtCr)
  
- [Ask questions about Flutter@Hashnode](https://hashnode.com/n/flutter)
